defmodule CalorieCounting do
  # Part 1
  def get_max(int_list), do: Enum.reduce(int_list, &Kernel.max(&1, &2))

  # Part 2: The recursive way
  def sum_max_n(int_list, n), do: sum_max_n(0, int_list, n)

  def sum_max_n(sum, _, 0), do: sum

  def sum_max_n(sum, int_list, n) do
    with [max, list_without_max] = find_and_remove_max(int_list) do
      sum_max_n(sum + max, list_without_max, n - 1)
    end
  end

  def find_and_remove_max(int_list) do
    with max = get_max(int_list) do
      [max, Enum.filter(int_list, &(&1 !== max))]
    end
  end

  # Part 2: The sorting way
  def sum_max_n_with_sort(int_list) do
    int_list
    |> Enum.sort(:desc)
    |> Enum.slice(0..2)
    |> Enum.reduce(&(&1 + &2))
  end

  # Solvers
  def solve_part_1 do
    solve(&get_max/1)
  end

  def solve_part_2 do
    solve(&sum_max_n(&1, 3))
  end

  def solve_part_2_with_sort do
    solve(&sum_max_n_with_sort/1)
  end

  def solve(solver) do
    with {:ok, content} = File.read("inputs/01_input.txt") do
      content
      |> String.split("\n\n", trim: true)
      |> Enum.map(&sum_string_list(&1))
      |> solver.()
    end
  end

  def sum_string_list(string_list) do
    string_list
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer(&1))
    |> Enum.reduce(&(&1 + &2))
  end
end
