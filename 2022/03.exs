defmodule Rucksack do
  def get_item_priority(item) do
    item
    |> String.to_charlist()
    |> hd
    |> ascii_to_priority()
  end

  def ascii_to_priority(ascii) do
    cond do
      # a-z have priorities starting at 1
      ascii >= ?a && ascii <= ?z -> ascii - (?a - 1)
      # A-Z have priorities starting at 27
      ascii >= ?A && ascii <= ?Z -> ascii - (?A - 27)
    end
  end

  # Part 1
  def get_repeated_item_priority(items) do
    items
    |> Enum.split(trunc(length(items) / 2))
    |> get_repeated_item()
    |> get_item_priority()
  end

  def get_repeated_item({firstHalf, secondHalf}) do
    [item] = Enum.uniq(firstHalf -- firstHalf -- secondHalf)
    item
  end

  # Part 2
  def get_group_repeated_item_priority([items1, items2, items3]) do
    with i1 = items1 -- items1 -- items2 do
      [item] = Enum.uniq(i1 -- i1 -- items3)
      get_item_priority(item)
    end
  end

  # Solvers
  def solve_part_1 do
    with {:ok, content} = File.read("inputs/03_input.txt") do
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.split(&1, "", trim: true))
      |> Enum.map(&get_repeated_item_priority/1)
      |> Enum.reduce(&(&1 + &2))
    end
  end

  def solve_part_2 do
    with {:ok, content} = File.read("inputs/03_input.txt") do
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.split(&1, "", trim: true))
      |> Enum.map(&Enum.uniq/1)
      |> Enum.chunk_every(3)
      |> Enum.map(&get_group_repeated_item_priority/1)
      |> Enum.reduce(&(&1 + &2))
    end
  end
end
