defmodule RockPaperScissors do
  @code_to_move %{
    "A" => :rock,
    "B" => :paper,
    "C" => :scissors,
    "X" => :rock,
    "Y" => :paper,
    "Z" => :scissors
  }

  @code_to_outcome %{
    "X" => :loss,
    "Y" => :draw,
    "Z" => :win
  }

  @move_to_points %{
    rock: 1,
    paper: 2,
    scissors: 3
  }

  @outcome_to_points %{
    win: 6,
    draw: 3,
    loss: 0
  }

  # Part 1 scoring
  def move_to_score1(str) do
    with %{"opponent_move" => opponent_move, "player_move" => player_move} =
           Regex.named_captures(~r{(?<opponent_move>[ABC]) (?<player_move>[XYZ])}, str) do
      score_round(@code_to_move[player_move], @code_to_move[opponent_move])
    end
  end

  def score_round(move, move),
    do: @move_to_points[move] + @outcome_to_points.draw

  def score_round(player_move, opponent_move),
    do: @move_to_points[player_move] + score_outcome(player_move, opponent_move)

  def score_outcome(:rock, :paper), do: @outcome_to_points.loss
  def score_outcome(:rock, :scissors), do: @outcome_to_points.win
  def score_outcome(:paper, :scissors), do: @outcome_to_points.loss
  def score_outcome(:paper, :rock), do: @outcome_to_points.win
  def score_outcome(:scissors, :rock), do: @outcome_to_points.loss
  def score_outcome(:scissors, :paper), do: @outcome_to_points.win

  # Part 2 scoring
  def move_to_score2(str) do
    with %{"opponent_move" => opponent_move, "outcome" => outcome} =
           Regex.named_captures(~r{(?<opponent_move>[ABC]) (?<outcome>[XYZ])}, str) do
      score_round2(@code_to_move[opponent_move], @code_to_outcome[outcome])
    end
  end

  def score_round2(opponent_move, :draw),
    do: @move_to_points[opponent_move] + @outcome_to_points.draw

  def score_round2(opponent_move, :win) do
    move_score =
      case opponent_move do
        :rock -> @move_to_points.paper
        :paper -> @move_to_points.scissors
        :scissors -> @move_to_points.rock
      end

    move_score + @outcome_to_points.win
  end

  def score_round2(opponent_move, :loss) do
    move_score =
      case opponent_move do
        :rock -> @move_to_points.scissors
        :paper -> @move_to_points.rock
        :scissors -> @move_to_points.paper
      end

    move_score + @outcome_to_points.loss
  end

  # Solvers
  def solve_part_1 do
    solve(&move_to_score1(&1))
  end

  def solve_part_2 do
    solve(&move_to_score2(&1))
  end

  def solve(score) do
    with {:ok, content} = File.read("inputs/02_input.txt") do
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&score.(&1))
      |> Enum.reduce(&(&1 + &2))
    end
  end
end
