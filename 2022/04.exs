defmodule CampCleanup do
  def assignment_to_range(assignment) do
    with %{"start" => range_start_str, "end" => range_end_str} =
           Regex.named_captures(~r{(?<start>\d+)-(?<end>\d+)}, assignment),
         range_start = String.to_integer(range_start_str),
         range_end = String.to_integer(range_end_str) do
      range_start..range_end
    end
  end

  def assignment_to_rangelist(assignment) do
    assignment
    |> assignment_to_range()
    |> Enum.to_list()
  end

  # Part 1
  def are_ranges_fully_overlapping([assignment1, assignment2]) do
    with range1 = assignment_to_rangelist(assignment1),
         range2 = assignment_to_rangelist(assignment2) do
      range1 -- range2 == [] || range2 -- range1 == []
    end
  end

  # Part 2
  def are_ranges_overlapping([assignment1, assignment2]) do
    with range1 = assignment_to_range(assignment1),
         range2 = assignment_to_range(assignment2) do
      !Range.disjoint?(range1, range2)
    end
  end

  # Solvers
  def solve_part_1 do
    solve(&are_ranges_fully_overlapping/1)
  end

  def solve_part_2 do
    solve(&are_ranges_overlapping/1)
  end

  def solve(solver) do
    with {:ok, content} = File.read("inputs/04_input.txt") do
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.split(&1, ",", trim: true))
      |> Enum.map(&solver.(&1))
      |> Enum.filter(&Function.identity(&1))
      |> length()
    end
  end
end
